## Asperger Survival Guide

["Coping: A Survival Guide for People with Asperger Syndrome" by Marc Segar](https://www-users.cs.york.ac.uk/~alistair/survival/).

Another online open book is: [Autistic Survival Guide](https://en.wikibooks.org/wiki/Autistic_Survival_Guide)

> "All things being equal, the simplest solution tends to be the best one" -Occam's Razor.
