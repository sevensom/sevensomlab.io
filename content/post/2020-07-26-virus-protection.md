---
title: Virus
subtitle: Protection
date: 2020-07-26
tags: ["virus"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

I have found methods to cope with the 2020 flu pandemic.

<!--more-->

## 1918 "Spanish flu":

"Cities were affected worse than rural areas."

"Social status was also reflected in the higher mortality among immigrant communities, with Italian Americans, a recently arrived group at the time, were nearly twice as likely to die compared to the average Americans.These disparities reflected worse diets, **crowded living conditions**".
[the pandemic](https://en.wikipedia.org/wiki/Spanish_flu)
```
Spinney, Laura. (2018). Pale rider : the Spanish flu of 1918 
and how it changed the world. Vintage. pp. 180–182. 
ISBN 978-1-78470-240-3. OCLC 1090305029.
```

"By the 1850s, [Florence Nightingale](https://en.wikipedia.org/wiki/Florence_Nightingale) was writing about the importance of sunlight and copious amounts of fresh air in the recovery of hospital patients,26,27 but her ideas were slow to gain acceptance." [The Open-Air Treatment of PANDEMIC INFLUENZA](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4504358/).

"The Massachusetts State Guard responded by building the Camp Brooks Open Air Hospital at Corey Hill in Brookline, near Boston. The hospital comprised 13 tents, 12 of which were occupied by one or two patients each and the other by the head nurse. The State Guard took seven hours to erect the tents, make sure the site was properly drained, and provide running water, latrines, and sewerage. Portable buildings were then set up for the medical staff and nurses. From the time the camp opened on September 9, 1918, until its closure a month later on October 12, a total of 351 victims of the pandemic were admitted, one third of whom were diagnosed with pneumonia. In total, 36 of the 351 sailors received at the hospital died."

"The treatment at Camp Brooks Hospital took place outdoors, with “a maximum of sunshine and of fresh air day and night." -> [PDF The Open-Air Treatment of PANDEMIC INFLUENZA](https://ajph.aphapublications.org/doi/pdf/10.2105/AJPH.2008.134627)

"The patients at Camp Brooks recovered in direct sunlight when available. This may have kept infection rates down, because laboratory experiments have shown that ultraviolet radiation inactivates influenza virus and other viral pathogens and that sunlight kills bacteria."



## [Mind-Body Help for the cold season, Self-Realization 2005](https://www.yogananda-srf.org/uploadedFiles/news/Self-Realization-Magazine/srm_wi05.pdf):

### Watch your diet carefully
"At least once every month you should give a thorough house-cleaning to your body by fasting."

"The greatest way to maintain health, is to fast on orange juice for two days once a month."

Fasting with citrus juices is excellent for prevention of colds, but not during a cold, as they prodcue mucus.
To rid yourself of a cold, it is better to fast on fresh, ripe, non-citrus fruits and ground raw almonds (unsalted).

Reduce your intake of unhealthful foods (such as meat, refined flour and sugar).

Increase your consumption of vitamin-rich fresh fruits and vegetables.

### Get Daily Exercise
Exercising regularly also helps the body resist colds and other infections. Walk, jog,... every day until perspiration breaks out over the whole body.

This not only elimnates harmful poisons through the pores of the skin, but raises the body temperature enough to destroy harmful bacteria, in the same way a fever does for a sick person.

Moderate exercise seems to improve immune function.

### Sunshine to Strengthen the Body's Life Force
Daily sunbaths for 10 to 30 minutes (depending on the intensity of the sun and sensitivity of the skin).

Sunlight triggers the body to make its own Vitamin D, crucial for keeping the immune system healthy.

Ultraviolet light increases the number of white blood cells or lymphocytes.

### Saltwater Hygiene for Throat and Sinuses
Gargling daily with a misture of half teaspoonful of salt in a glass of warm water early in the morning, at noon, and just before going to bed. 
Nasal wash as well.

The flow of salt water cleanses away unhealthy dirt, pollutans, and microbe-harboring mucus.

## Additional information:

[Sea salt could help beat a cold, study suggests](https://www.ed.ac.uk/news/2019/sea-salt-could-help-beat-a-cold-study-suggests)
[A pilot, open labelled, randomised controlled trial of hypertonic saline nasal irrigation and gargling for the common cold](https://www.nature.com/articles/s41598-018-37703-3#Sec2) Published: 31 January 2019, they recruited 68 participants over 26 weeks between October 2014 and March 2015:

"These finding along with the lower rate of symptomatic household contacts in the intervention arm suggest that HSNIG (hypertonic saline nasal irrigation and gargling) helps reduce viral replication. Since viruses are shed during breathing and speaking7, measure that helps reduce viral shedding would help reduce transmission."

"In cell culture models, DNA, RNA, enveloped and non-enveloped viruses are all inhibited in the presence of NaCl15. The antiviral effect is dependent on the availability of chloride ions (and not sodium ions)15. In the presence viral infection and the availability of NaCl, cells utilise the chloride ions to produce hypochlorous acid (HOCl)15. Since HOCl is the active ingredient in bleach, which is known to have an antiviral effect, the mechanism could be augmented by supplying chloride ions through NaCl to treat infections." [Article link](https://rdcu.be/b5Q22)

```
Ramalingam, S., Cai, B., Wong, J. et al. Antiviral innate immune 
response in non-myeloid cells is augmented by chloride ions via an 
increase in intracellular hypochlorous acid levels. Sci Rep 8, 13630 
(2018).
```
