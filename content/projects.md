---
title: Projects
subtitle: Stuff I've done
---

# Web development 

### [Web Design and Marketing] (http://loliladelcolorao.es/)

{{< figure src="/img/2.jpg" caption="Responsive web development" caption-position="bottom" caption-effect="appear" >}}

  
### [Development and Copywriting] (http://apartamentoschimenea.com/)

{{< figure src="/img/3.jpg" caption="WordPress" caption-position="bottom" caption-effect="appear" >}}

  
### [Web Development] (http://nerjavacaciones.es/)

{{< figure src="/img/4.jpg" caption="Bootstrap" caption-position="bottom" caption-effect="appear" >}}


### [Gata Cattana Fansite] (https://codepen.io/somval/full/eKOVdX)

{{< figure src="https://farm3.staticflickr.com/2892/33405508361_49cc750511_k.jpg" caption="Bootstrap 4" caption-position="bottom" caption-effect="appear" >}}


### [Web app Development] (https://somakari.github.io/farmacia/)

{{< figure src="/img/farmacia.jpg" caption="Web app" caption-position="bottom" caption-effect="appear" >}}

# Photography, Graphic Design, Lettering

{{< gallery >}}
{{< figure link="/img/galeria/24338095107_49ac11d8b4_b.jpg" >}}
{{< figure link="/img/galeria/27421394159_d817bfffbd_c.jpg" >}}
{{< figure link="/img/galeria/38321246525_9f7dce31d3_c.jpg" >}}
{{< figure link="/img/galeria/38322565165_09af1b2645_b.jpg" >}}
{{< figure link="/img/galeria/39199109941_cf37d3c53a_b.jpg" >}}
{{< figure link="/img/galeria/45226109742_e8b3c0f0f3_b.jpg" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

# Illustration
