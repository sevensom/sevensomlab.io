---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

My name is The Dude. I have the following qualities:

<i class="fas fa-graduation-cap"></i>   Environmental Scientist & Higher Technician in Computer Network Systems Management.

<i class="fas fa-code"></i>     HTML, CSS, Javascript, SQL, PHP, WordPress, and planning to learn Python.

<i class="fas fa-language"></i>     I speak English and Spanish, I learnt French in highschool, and I'm learning Mandarin.

<i class="fas fa-heart"></i>    I love going into nature as much as possible, photography, drawing, running and swimming.

<i class="fas fa-music"></i>    I play Steel Tongue Drum from RAV (D Major). You can check out some of my recordings on [YouTube] (https://www.youtube.com/loveonolove).



### My history

To be honest, I'm having some trouble remembering right now.

{{< youtube TYJ1LL3J22I >}}


### How about a yummy Gluten-free crepe?

![Crepe](http://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg)

